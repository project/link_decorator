# Link Decorator Module

## Overview

The Link Decorator module is designed to enhance the appearance and functionality of links on your Drupal site. It provides a field formatter for the core link field, allowing you to decorate links with SVG icons and validate link ownership based on the `rel=me` link attribute.

## Features

- Decorate link field's links with SVG icons (e.g., social links).
- Add custom SVG icons for links based on your own regular expression.
- Verify link ownership using Webfinger and/or Microformats.
  Link ownership verification works well with platforms like Mastodon, Gitlab, Github, and others implementing Webfinger and/or Microformats.

## Post-Installation

1. Add link decorators via configuration.
2. Apply decorators with the decorated links field formatter to any link field on a user or a node entity.

## Similar Projects

- [Social Media Links Block and Field](https://www.drupal.org/project/social_media_links): Configurable block displaying links (icons) to your profiles on various networking sites.
- [Social Links API](https://www.drupal.org/project/social_links): Light-weight social share links to attach to your entities.
- [Social Link Field](https://www.drupal.org/project/social_link_field): Provides a social link field type.
- [SocialLinks](https://www.drupal.org/project/sociallinks_online): Easier addition of social media links to your organization's Drupal Website.

## Maintainers

- Stefan Auditor - sanduhrs - https://www.drupal.org/u/sanduhrs
