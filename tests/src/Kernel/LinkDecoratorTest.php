<?php

declare(strict_types = 1);

namespace Drupal\Tests\link_decorator\Kernel;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;

/**
 * Basic field formatter test.
 *
 * @group link_decorator
 */
final class LinkDecoratorTest extends FieldKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['link', 'link_decorator', 'entity_test'];

  /**
   * The entity type test.
   *
   * @var string
   */
  protected $entityType;

  /**
   * The entity test bundle.
   *
   * @var string
   */
  protected $bundle;

  /**
   * The image test field name.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * The field display.
   *
   * @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface
   */
  protected $display;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['field', 'link_decorator']);
    $this->installEntitySchema('entity_test');

    $this->entityType = 'entity_test';
    $this->bundle = $this->entityType;
    $this->fieldName = mb_strtolower($this->randomMachineName());

    FieldStorageConfig::create([
      'entity_type' => $this->entityType,
      'field_name' => $this->fieldName,
      'type' => 'link',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
    ])->save();
    FieldConfig::create([
      'entity_type' => $this->entityType,
      'field_name' => $this->fieldName,
      'bundle' => $this->bundle,
      'settings' => [
        'target' => '_blank',
        'rel_me' => 'me',
        'rel_nofollow' => 'nofollow',
        'rel_noopener' => 'noopener',
        'rel_noreferrer' => 'noreferrer',
        'verify' => 'verify',
      ],
    ])->save();

    $this->display = \Drupal::service('entity_display.repository')
      ->getViewDisplay($this->entityType, $this->bundle)
      ->setComponent($this->fieldName, [
        'type' => 'link_decorator_links',
      ]);
    $this->display->save();
  }

  /**
   * Test callback.
   */
  public function testFormatter(): void {
    $link_decorators = \Drupal::entityTypeManager()
      ->getStorage('link_decorator')
      ->loadMultiple();
    foreach ($link_decorators as $link_decorator) {
      $entity = EntityTest::create([
        'name' => $this->randomMachineName(),
        $this->fieldName => [
          ['uri' => $link_decorator->get('link')],
        ],
      ]);
      $entity->save();

      $build = $this->display->build($entity);
      $content = $this->container->get('renderer')->renderRoot($build)->__toString();

      $this->assertStringContainsString('class="' . $link_decorator->get('class'), $content);
      $this->assertStringContainsString('<svg', $content);
      $this->assertStringContainsString('unverified', $content);
    }
  }

}
