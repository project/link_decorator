<?php

declare(strict_types = 1);

namespace Drupal\link_decorator;

/**
 * Interface for verifier plugins.
 */
interface VerifierInterface {

  /**
   * Returns the translated plugin label.
   */
  public function label(): string;

  /**
   * Verifies an indentifer.
   *
   * @param string $uri
   *   An identifier in the form of 'user@example.org', '@user@example.org' or
   *   'https://example.org/user'.
   * @param string $local_uri
   *   An identifier in the form of 'user@example.org', '@user@example.org' or
   *   'https://example.org/user'.
   *
   * @return string
   *   A DATE_W3C formatted datetime string or an empty string on failure.
   */
  public function verify(string $uri, string $local_uri): int;

}
