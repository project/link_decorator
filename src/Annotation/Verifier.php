<?php

declare(strict_types = 1);

namespace Drupal\link_decorator\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines verifier annotation object.
 *
 * @Annotation
 */
final class Verifier extends Plugin {

  /**
   * The plugin ID.
   */
  public readonly string $id;

  /**
   * The human-readable name of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public readonly string $title;

  /**
   * The description of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public readonly string $description;

  /**
   * The plugin weight.
   */
  public readonly int $weight;

}
