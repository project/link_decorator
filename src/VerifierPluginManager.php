<?php

declare(strict_types = 1);

namespace Drupal\link_decorator;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\link_decorator\Annotation\Verifier;

/**
 * Verifier plugin manager.
 */
final class VerifierPluginManager extends DefaultPluginManager {

  /**
   * Constructs the object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Verifier', $namespaces, $module_handler, VerifierInterface::class, Verifier::class);
    $this->alterInfo('verifier_info');
    $this->setCacheBackend($cache_backend, 'verifier_plugins');
  }

}
