<?php

declare(strict_types = 1);

namespace Drupal\link_decorator\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\link_decorator\LinkDecoratorInterface;

/**
 * Defines the link decorator type.
 *
 * @ConfigEntityType(
 *   id = "link_decorator",
 *   label = @Translation("Link Decorator"),
 *   label_collection = @Translation("Link Decorators"),
 *   label_singular = @Translation("link decorator"),
 *   label_plural = @Translation("link decorators"),
 *   label_count = @PluralTranslation(
 *     singular = "@count link decorator",
 *     plural = "@count link decorators",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\link_decorator\LinkDecoratorListBuilder",
 *     "form" = {
 *       "add" = "Drupal\link_decorator\Form\LinkDecoratorForm",
 *       "edit" = "Drupal\link_decorator\Form\LinkDecoratorForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "link_decorator",
 *   admin_permission = "administer link_decorator",
 *   links = {
 *     "collection" = "/admin/structure/link-decorator",
 *     "add-form" = "/admin/structure/link-decorator/add",
 *     "edit-form" = "/admin/structure/link-decorator/{link_decorator}",
 *     "delete-form" = "/admin/structure/link-decorator/{link_decorator}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "regex",
 *     "link",
 *     "title",
 *     "icon",
 *     "class",
 *     "verify",
 *   },
 * )
 */
final class LinkDecorator extends ConfigEntityBase implements LinkDecoratorInterface {

  /**
   * The id attribute.
   */
  protected string $id;

  /**
   * The label attribute.
   */
  protected string $label;

  /**
   * The description attribute.
   */
  protected string $regex;

  /**
   * The link attribute.
   */
  protected string $link;

  /**
   * The description attribute.
   */
  protected string $title;

  /**
   * The description attribute.
   */
  protected string $icon;

  /**
   * The description attribute.
   */
  protected string $class;

  /**
   * The verify attribute.
   */
  protected bool $verify;

}
