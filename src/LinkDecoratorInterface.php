<?php

declare(strict_types = 1);

namespace Drupal\link_decorator;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a link decorator entity type.
 */
interface LinkDecoratorInterface extends ConfigEntityInterface {

}
