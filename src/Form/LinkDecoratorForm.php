<?php

declare(strict_types = 1);

namespace Drupal\link_decorator\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link_decorator\Entity\LinkDecorator;

/**
 * Link Decorator form.
 */
final class LinkDecoratorForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [LinkDecorator::class, 'load'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];
    $form['link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Example link'),
      '#description' => $this->t('The regular expression to identify the provider from a URL.'),
      '#default_value' => $this->entity->get('link'),
    ];
    $form['regex'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Regular expression'),
      '#description' => $this->t('The regular expression to identify the provider from a URL, e.g. <em>^https?:\/\/((example)\.org)\/?([a-z-]*)</em>.'),
      '#default_value' => $this->entity->get('regex'),
    ];
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title pattern'),
      '#description' => $this->t('The link title pattern with replacement tokens to use for this link, e.g. the string <em>@group1 (@group0)</em> might result in <em>Example (example.org)</em>.'),
      '#default_value' => $this->entity->get('title'),
    ];
    if ($this->entity->get('link') && $this->entity->get('regex')) {
      $description = '';
      $description .= $this->t('Based on the fields <em>Example link</em> and <em>Regular expression</em> above, the following tokens are available for substitution:');
      preg_match('/' . $this->entity->get('regex') . '/', $this->entity->get('link'), $matches);
      $description .= '<dl>';
      foreach ($matches as $id => $match) {
        $description .= '<dl><dt><strong>@group' . $id . '</strong><dt><dd>' . $match . '</dd></dl>';
      }
      $description .= 'The string <em>@group1 (@group0)</em> will result in <em>' . ucfirst(($matches[1] ?? '') . ' (' . ($matches[0] ?? '') . ')</em>.');
      $description .= '</dl>';
      $form['title']['#description'] = $description;
    }
    $form['icon'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Icon'),
      '#default_value' => $this->entity->get('icon'),
      '#description' => $this->t('Must be SVG compatible XML markup, e.g.  <code>&lt;svg version="1.1" xmlns="http://www.w3.org/2000/svg"&gt;&lt;svg&gt;&lt;defs /&gt;&lt;path /&gt;&lt;/svg&gt;</code>.'),
    ];
    $form['class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Class'),
      '#description' => $this->t('The CSS class to append to the element.'),
      '#default_value' => $this->entity->get('class'),
    ];
    $form['verify'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Verify'),
      '#default_value' => $this->entity->get('verify'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $element_name = 'icon';
    $source = $form_state->getValue($element_name);

    // @See: http://php.net/manual/en/function.libxml-use-internal-errors.php
    libxml_use_internal_errors(TRUE);
    $dom = simplexml_load_string($source);
    // Array with LibXMLError objects if there are any errors in the buffer, or
    // an empty array otherwise.
    $svg_errors = libxml_get_errors();

    if ($dom === FALSE || $svg_errors) {
      $form_state->setErrorByName($element_name, $this->t('The value has to be a valid xml string.'));

      // Iterate over errors generated by an invalid svg source.
      foreach ($svg_errors as $svg_error) {
        $message = $this->t('%message [line %line, column %column]', [
          '%message' => $svg_error->message,
          '%line' => $svg_error->line,
          '%column' => $svg_error->column,
        ]);
        $form_state->setErrorByName($element_name, $this->t('SVG failed validation: @error', ['@error' => $message]));
      }
    }
    else {
      // Check if the current SVG contain at least one valid symbol definition.
      $path = $dom->path;
      if (!count($path)) {
        $form_state->setErrorByName($element_name, $this->t('The SVG must contain at least one valid path.'));
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $this->messenger()->addStatus(
      match($result) {
        \SAVED_NEW => $this->t('Created new example %label.', $message_args),
        \SAVED_UPDATED => $this->t('Updated example %label.', $message_args),
      }
    );
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
