<?php
//phpcs:ignoreFile

namespace Drupal\link_decorator\Mf2;

/**
 * A \MF2\fetch() override.
 *
 * Needed to add a useragent string that allows to bypass d.o captcha mechanism.
 */
class Fetcher {

  const USERAGENT = 'Mozilla/5.0 (X11; Linux x86_64; rv:122.0) Gecko/20100101 Firefox/122.0';

  /**
   * Fetch microformats2.
   *
   * Given a URL, fetches it (following up to 5 redirects) and, if the content-type appears to be HTML, returns the parsed
   * microformats2 array structure.
   *
   * Not that even if the response code was a 4XX or 5XX error, if the content-type is HTML-like then it will be parsed
   * all the same, as there are legitimate cases where error pages might contain useful microformats (for example a deleted
   * h-entry resulting in a 410 Gone page with a stub h-entry explaining the reason for deletion). Look in $curlInfo['http_code']
   * for the actual value.
   *
   * @param string $url The URL to fetch
   * @param bool $convertClassic (optional, default true) whether or not to convert classic microformats
   * @param &array $curlInfo (optional) the results of curl_getinfo will be placed in this variable for debugging
   * @return array|null canonical microformats2 array structure on success, null on failure
   */
  public static function fetch($url, $convertClassic = true, &$curlInfo=null) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_USERAGENT, static::USERAGENT);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Accept: text/html'
    ));
    $html = curl_exec($ch);
    $info = $curlInfo = curl_getinfo($ch);
    curl_close($ch);

    if (strpos(strtolower($info['content_type']), 'html') === false) {
        // The content was not delivered as HTML, do not attempt to parse it.
        return null;
    }

    # ensure the final URL is used to resolve relative URLs
    $url = $info['url'];

    return \Mf2\parse($html, $url, $convertClassic);
  }

}
