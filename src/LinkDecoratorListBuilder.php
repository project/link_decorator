<?php

declare(strict_types = 1);

namespace Drupal\link_decorator;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of link decorators.
 */
final class LinkDecoratorListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['icon'] = $this->t('Icon');
    $header['label'] = $this->t('Label');
    $header['link_title'] = $this->t('Link title');
    $header['id'] = $this->t('Machine name');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\link_decorator\LinkDecoratorInterface $entity */
    $row['svg']['data'] = $this->buildIcon($entity);
    $row['label'] = $entity->label();
    $row['linkt_title']['data'] = $this->buildLinkTitle($entity);
    $row['id'] = $entity->id();
    $row['status'] = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');
    return $row + parent::buildRow($entity);
  }

  /**
   * Builds a svg icon for the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity on which the linked operations will be performed.
   *
   * @return array
   *   A renderable array of the svg icon.
   *
   * @see \Drupal\Core\Entity\EntityListBuilder::buildRow()
   */
  protected function buildIcon(EntityInterface $entity) {
    $build = [
      '#type' => 'inline_template',
      '#template' => $entity->get('icon'),
    ];
    $build['#attached']['library'][] = 'link_decorator/list-builder';
    return $build;
  }

  /**
   * Builds a link title for the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity on which the linked operations will be performed.
   *
   * @return array
   *   A renderable array of the link title.
   *
   * @see \Drupal\Core\Entity\EntityListBuilder::buildRow()
   * @todo Fix duplicate code by refactoring into a central service.
   */
  protected function buildLinkTitle(EntityInterface $entity) {
    preg_match('/' . $entity->get('regex') . '/', $entity->get('link'), $matches);

    $title = $entity->get('title');
    $replacements = [];
    foreach ($matches as $id => $match) {
      $replacements['@group' . $id] = $match;
    }

    $title_processed = new FormattableMarkup($title, $replacements);

    $build = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#value' => ucfirst($title_processed->__toString()),
    ];
    return $build;
  }

}
