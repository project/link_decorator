<?php

declare(strict_types = 1);

namespace Drupal\link_decorator\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a verification block.
 *
 * @Block(
 *   id = "link_decorator_verification",
 *   admin_label = @Translation("Link verification"),
 *   category = @Translation("Link decoration"),
 * )
 */
final class VerificationBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs the plugin instance.
   */
  public function __construct(
      array $configuration,
      $plugin_id,
      $plugin_definition,
      private readonly ConfigFactoryInterface $configFactory,
      private readonly CurrentRouteMatch $routeMatch
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
      ContainerInterface $container,
      array $configuration,
      $plugin_id,
      $plugin_definition
  ): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    if (!$entity = $this->getRouteEntity()) {
      return [];
    }

    $system_site = $this->configFactory->get('system.site');
    $build['content'] = [
      '#markup' => $this->t('You can verify yourself as the owner of the links. For that, the linked website must contain a link back to this page. The link back must have a <code>rel="me"</code> attribute. The text content of the link does not matter. Here is an example: <pre class="scroll">&lt;a rel="me" href="@uri"&gt;@site_name&lt;/a&gt;</pre>', [
        '@uri' => $entity->toUrl('canonical')->setAbsolute()->toString(TRUE)->getGeneratedUrl(),
        '@site_name' => $system_site->get('name'),
      ]),
    ];
    $build['#attached']['library'][] = 'link_decorator/verification-block';
    return $build;
  }

  /**
   * Helper function to extract the entity for the supplied route.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   A content entity or null.
   */
  private function getRouteEntity() {
    // Entity will be found in the route parameters.
    if (($route = $this->routeMatch->getRouteObject()) && ($parameters = $route->getOption('parameters'))) {
      // Determine if the current route represents an entity.
      foreach ($parameters as $name => $options) {
        if (isset($options['type']) && strpos($options['type'], 'entity:') === 0) {
          $entity = $this->routeMatch->getParameter($name);
          if ($entity instanceof ContentEntityInterface && $entity->hasLinkTemplate('canonical')) {
            return $entity;
          }

          // Since entity was found, no need to iterate further.
          return NULL;
        }
      }
    }
  }

}
