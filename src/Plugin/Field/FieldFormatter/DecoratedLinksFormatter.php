<?php

declare(strict_types = 1);

namespace Drupal\link_decorator\Plugin\Field\FieldFormatter;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Url;
use Drupal\link\LinkItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Decorated Links' formatter.
 *
 * @FieldFormatter(
 *   id = "link_decorator_links",
 *   label = @Translation("Decorated Links"),
 *   field_types = {"link"},
 * )
 */
final class DecoratedLinksFormatter extends FormatterBase {

  /**
   * The path validator service.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The verifier plugin manager service.
   *
   * @var \Drupal\link_decorator\VerifierPluginManager
   */
  protected $verifierPluginManager;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(
      ContainerInterface $container,
      array $configuration,
      $plugin_id,
      $plugin_definition
  ) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('path.validator'),
      $container->get('token'),
      $container->get('date.formatter'),
      $container->get('plugin.manager.verifier'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Constructs a new LinkFormatter.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Third party settings.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
   *   The path validator service.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Datetime\DateFormatter $verifier_plugin_manager
   *   The verifier plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(
      $plugin_id,
      $plugin_definition,
      FieldDefinitionInterface $field_definition,
      array $settings,
      $label,
      $view_mode,
      array $third_party_settings,
      PathValidatorInterface $path_validator,
      $token,
      $date_formatter,
      $verifier_plugin_manager,
      $entity_type_manager
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings
    );
    $this->pathValidator = $path_validator;
    $this->token = $token;
    $this->dateFormatter = $date_formatter;
    $this->verifierPluginManager = $verifier_plugin_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'trim_length' => '80',
      'url_only' => '',
      'url_plain' => '',
      'target' => '_blank',
      'rel_me' => 'me',
      'rel_nofollow' => 'nofollow',
      'rel_noopener' => 'noopener',
      'rel_noreferrer' => 'noreferrer',
      'verify' => 'verify',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['trim_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Trim link text length'),
      '#field_suffix' => $this->t('characters'),
      '#default_value' => $this->getSetting('trim_length'),
      '#min' => 1,
      '#description' => $this->t('Leave blank to allow unlimited link text lengths.'),
    ];
    $elements['url_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('URL only'),
      '#default_value' => $this->getSetting('url_only'),
      '#access' => $this->getPluginId() == 'link',
    ];
    $elements['url_plain'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show URL as plain text'),
      '#default_value' => $this->getSetting('url_plain'),
      '#access' => $this->getPluginId() == 'link',
      '#states' => [
        'visible' => [
          ':input[name*="url_only"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $elements['target'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Open link in new window'),
      '#return_value' => '_blank',
      '#default_value' => $this->getSetting('target'),
    ];
    $elements['rel_me'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add rel="me" to links'),
      '#return_value' => 'me',
      '#default_value' => $this->getSetting('rel_me'),
    ];
    $elements['rel_nofollow'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add rel="nofollow" to links'),
      '#return_value' => 'nofollow',
      '#default_value' => $this->getSetting('rel_nofollow'),
    ];
    $elements['rel_noopener'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add rel="noopener" to links'),
      '#return_value' => 'noopener',
      '#default_value' => $this->getSetting('rel_noopener'),
    ];
    $elements['rel_noreferrer'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add rel="noreferrer" to links'),
      '#return_value' => 'noreferrer',
      '#default_value' => $this->getSetting('rel_noreferrer'),
    ];
    $elements['verify'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Verify links'),
      '#return_value' => 'verify',
      '#default_value' => $this->getSetting('verify'),
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $settings = $this->getSettings();

    if (!empty($settings['trim_length'])) {
      $summary[] = $this->t('Link text trimmed to @limit characters', ['@limit' => $settings['trim_length']]);
    }
    else {
      $summary[] = $this->t('Link text not trimmed');
    }
    if ($this->getPluginId() == 'link' && !empty($settings['url_only'])) {
      if (!empty($settings['url_plain'])) {
        $summary[] = $this->t('Show URL only as plain-text');
      }
      else {
        $summary[] = $this->t('Show URL only');
      }
    }
    if (!empty($settings['rel_me'])) {
      $summary[] = $this->t('Add rel="@rel"', ['@rel' => $settings['rel_me']]);
    }
    if (!empty($settings['rel_nofollow'])) {
      $summary[] = $this->t('Add rel="@rel"', ['@rel' => $settings['rel_nofollow']]);
    }
    if (!empty($settings['rel_noopener'])) {
      $summary[] = $this->t('Add rel="@rel"', ['@rel' => $settings['rel_noopener']]);
    }
    if (!empty($settings['rel_noreferrer'])) {
      $summary[] = $this->t('Add rel="@rel"', ['@rel' => $settings['rel_noreferrer']]);
    }
    if (!empty($settings['target'])) {
      $summary[] = $this->t('Open link in new window');
    }
    if (!empty($settings['verify'])) {
      $summary[] = $this->t('Verify links');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $entity = $items->getEntity();
    $settings = $this->getSettings();

    foreach ($items as $delta => $item) {
      // By default use the full URL as the link text.
      $url = $this->buildUrl($item);
      $link_title = $url->toString();

      $decoration = $this->decorate($url->toString(), $entity, TRUE);
      $verified = $decoration['verified'];

      // If the title field value is available, use it for the link text.
      if (empty($settings['url_only']) && !empty($item->title)) {
        // Unsanitized token replacement here because the entire link title
        // gets auto-escaped during link generation in
        // \Drupal\Core\Utility\LinkGenerator::generate().
        $link_title = $this->token->replace($item->title, [$entity->getEntityTypeId() => $entity], ['clear' => TRUE]);
      }

      // Trim the link text to the desired length.
      if (!empty($settings['trim_length'])) {
        $link_title = Unicode::truncate($link_title, $settings['trim_length'], FALSE, TRUE);
      }

      if (!empty($settings['url_only']) && !empty($settings['url_plain'])) {
        $element[$delta] = [
          '#plain_text' => $link_title,
        ];

        if (!empty($item->_attributes)) {
          // Piggyback on the metadata attributes, which will be placed in the
          // field template wrapper, and set the URL value in a content
          // attribute.
          $content = str_replace('internal:/', '', $item->uri);
          $item->_attributes += ['content' => $content];
        }
      }
      else {
        $classes = implode(' ', [$decoration['class'], ($verified ? 'verified' : 'unverified')]);
        $element[$delta]['#prefix'] = '<div class="' . $classes . '">';
        if (isset($decoration['icon'])) {
          $element[$delta]['icon'] = [
            '#type' => 'inline_template',
            '#template' => $decoration['icon'],
          ];
        }
        $element[$delta]['link'] = [
          '#type' => 'link',
          '#title' => $decoration['title'] ? ucfirst($decoration['title']) : $link_title,
          '#options' => $url->getOptions(),
        ];
        $element[$delta]['link']['#url'] = $url;
        if ($verified) {
          $element[$delta]['verified'] = [
            '#type' => 'html_tag',
            '#tag' => 'span',
            '#value' => $this->t('✓'),
            '#attributes' => [
              'title' => $this->t('Ownership of this link was checked on @datetime', [
                '@datetime' => $this->dateFormatter->format($verified, 'small'),
              ]),
              'data-checked' => $verified,
            ],
          ];
        }

        if (!empty($item->_attributes)) {
          $element[$delta]['link']['#options'] += ['attributes' => []];
          $element[$delta]['link']['#options']['attributes'] += $item->_attributes;
          // Unset field item attributes since they have been included in the
          // formatter output and should not be rendered in the field template.
          unset($item->_attributes);
        }
        $element[$delta]['#suffix'] = '</div>';
      }
      $element[$delta]['verified']['#attached']['library'][] = 'link_decorator/formatter';
    }

    return $element;
  }

  /**
   * Builds the \Drupal\Core\Url object for a link field item.
   *
   * @param \Drupal\link\LinkItemInterface $item
   *   The link field item being rendered.
   *
   * @return \Drupal\Core\Url
   *   A Url object.
   */
  protected function buildUrl(LinkItemInterface $item) {
    try {
      $url = $item->getUrl();
    }
    catch (\InvalidArgumentException $e) {
      // @todo Add logging here in https://www.drupal.org/project/drupal/issues/3348020
      $url = Url::fromRoute('<none>');
    }

    $settings = $this->getSettings();
    $options = $item->options;
    $options += $url->getOptions();

    // Add optional 'rel' attribute to link options.
    $options['attributes']['rel'] = '';
    if (!empty($settings['rel_me'])) {
      $options['attributes']['rel'] .= ' ' . $settings['rel_me'];
    }
    if (!empty($settings['rel_nofollow'])) {
      $options['attributes']['rel'] .= ' ' . $settings['rel_nofollow'];
    }
    if (!empty($settings['rel_noopener'])) {
      $options['attributes']['rel'] .= ' ' . $settings['rel_noopener'];
    }
    if (!empty($settings['rel_noreferrer'])) {
      $options['attributes']['rel'] .= ' ' . $settings['rel_noreferrer'];
    }
    $options['attributes']['rel'] = trim($options['attributes']['rel'], ' ');
    // Add optional 'target' attribute to link options.
    if (!empty($settings['target'])) {
      $options['attributes']['target'] = $settings['target'];
    }
    $url->setOptions($options);

    return $url;
  }

  /**
   * Verifies a URI.
   *
   * @return string
   *   A W3C_DATE formatted datetime string or an empty string.
   */
  private function verify(string $url, $entity) {
    $type = $this->verifierPluginManager;
    $plugin_definitions = $type->getDefinitions();
    foreach ($plugin_definitions as $plugin_definition) {
      $plugin = $type->createInstance($plugin_definition['id']);
      if ($verified = $plugin->verify($url, $entity->toUrl('canonical')->setAbsolute()->toString(TRUE)->getGeneratedUrl())) {
        return $verified;
      }
    }
    return '';
  }

  /**
   * Decorates a URI.
   *
   * @return array
   *   An associative array or an empty array.
   */
  private function decorate(string $url, $entity, $verify = FALSE) {
    $query = $this->entityTypeManager
      ->getStorage('link_decorator')
      ->getQuery('AND')
      ->condition('status', TRUE)
      ->accessCheck(FALSE);
    $results = $query->execute();
    foreach ($results as $id) {
      $link_decorator = $this->entityTypeManager
        ->getStorage('link_decorator')
        ->load($id);

      preg_match('/' . $link_decorator->get('regex') . '/', $url, $matches);
      if (!$matches) {
        continue;
      }

      $title = $link_decorator->get('title');
      $replacements = [];
      foreach ($matches as $id => $match) {
        $replacements['@group' . $id] = $match;
      }

      $title_processed = new FormattableMarkup($title, $replacements);
      return [
        'id' => $link_decorator->id(),
        'label' => $link_decorator->label(),
        'regex' => $link_decorator->get('regex'),
        'title' => $title_processed->__toString(),
        'icon' => $link_decorator->get('icon'),
        'class' => $link_decorator->get('class'),
        'verified' => $verify ? $this->verify($url, $entity) : '',
      ];
    }
    return [];
  }

}
