<?php

declare(strict_types = 1);

namespace Drupal\link_decorator\Plugin\Verifier;

use Drupal\link_decorator\Mf2\Fetcher;
use Drupal\link_decorator\VerifierPluginBase;

/**
 * Plugin implementation of the verifier.
 *
 * @Verifier(
 *   id = "microformats",
 *   label = @Translation("Microformats"),
 *   description = @Translation("Microformats verifier."),
 *   weight = 100
 * )
 */
final class MicroformatsVerifier extends VerifierPluginBase {

  /**
   * {@inheritdoc}
   */
  public function verify(string $uri, string $local_uri): int {
    $timestamp = 0;

    $cid = implode('-', ['link_decorator', get_class($this), crc32($uri)]);
    if ($cache = $this->cache->get($cid)) {
      return $cache->data;
    }

    $mf = Fetcher::fetch($uri);
    if (is_array($mf)
      && isset($mf['rels']['me'])) {
      foreach ($mf['rels']['me'] as $remote_uri) {
        if ($remote_uri === $local_uri) {
          $timestamp = \Drupal::time()->getRequestTime();
          break;
        }
      }
    }
    $this->cache->set($cid, $timestamp, \Drupal::time()->getRequestTime() + 60 * 5);
    return $timestamp;
  }

}
