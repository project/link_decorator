<?php

declare(strict_types = 1);

namespace Drupal\link_decorator\Plugin\Verifier;

use Drupal\link_decorator\Mf2\Fetcher;
use Drupal\link_decorator\VerifierPluginBase;

/**
 * Plugin implementation of the verifier.
 *
 * @Verifier(
 *   id = "webfinger",
 *   label = @Translation("Webfinger"),
 *   description = @Translation("Webfinger verifier."),
 *   weight = 200
 * )
 */
final class WebfingerVerifier extends VerifierPluginBase {

  /**
   * {@inheritdoc}
   */
  public function verify(string $uri, string $local_uri): int {
    $timestamp = 0;

    $cid = implode('-', ['link_decorator', get_class($this), crc32($uri)]);
    if ($cache = $this->cache->get($cid)) {
      return $cache->data;
    }

    $wf = new \Net_WebFinger();
    $react = $wf->finger($uri);
    if ($react->error || !$react->secure) {
      $this->cache->set($cid, $timestamp, \Drupal::time()->getRequestTime() + 60 * 5);

      // Those data may not be trusted.
      return 0;
    }

    foreach ($react as $link) {
      if ($link->rel === 'self') {
        $mf = Fetcher::fetch($link->href);
        if (is_array($mf)
          && isset($mf['rels']['me'])) {
          foreach ($mf['rels']['me'] as $href) {
            if ($href === $local_uri) {
              $timestamp = \Drupal::time()->getRequestTime();
            }
            break 2;
          }
        }
      }
    }
    $this->cache->set($cid, $timestamp, \Drupal::time()->getRequestTime() + 60 * 5);
    return $timestamp;
  }

}
